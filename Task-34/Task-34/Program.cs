﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            int weeks;

        Start: 

            Console.WriteLine("Please enter the number of weeks to calculate how many working days there are. (Numbers only).");

            if (int.TryParse(Console.ReadLine(), out weeks))
            {
                Console.WriteLine($"There are {weeks*5} working days in {weeks} weeks.");
            }

            goto Start;
        }
    }
}
