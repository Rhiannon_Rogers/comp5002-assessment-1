﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the month you were born (letters only).");
            var month = Console.ReadLine();
            Console.WriteLine("Please enter the day you were born (1-31).");
            var day = Console.ReadLine();
            Console.WriteLine($"You were born on day {day} of the month {month}.");
        }
    }
}
