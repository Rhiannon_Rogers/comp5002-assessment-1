﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;

            Start:

            Console.WriteLine("What year is it?");

            if (int.TryParse(Console.ReadLine(), out year))
            {
                Console.WriteLine("Leap years in the next 20 years are:");

                for (int counter = 0; counter < 21; counter++)
                {
                    if (year % 4 == 0 && (year % 400 == 0 || year % 100 > 0))
                    {
                        Console.WriteLine($"{year}");
                    }
                    year++;
                }
            }
   
            goto Start;
        }
    }
}
