﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {

        Start:

            Console.WriteLine("Please enter 3 words on the same line, separated by a space.");

            var input = Console.ReadLine();
            var output = input.Split(' ');

            if (output.Count() > 3)
            {
                Console.WriteLine($"You entered more than 3 words. You entered {output.Count()} words. Please try again.");
                goto Start;
            }

            Console.WriteLine("The words you entered were:");

            foreach (var item in output)
            {
                Console.WriteLine($"{item}");
            }

            goto Start;
        }
    }
}
