﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;

        Start:

            Console.WriteLine("Please enter a number to perform the division tables for. (Enter numbers only).");

            if (double.TryParse(Console.ReadLine(), out number))
            {
                for (int counter = 1; counter < 13; counter++)
                {
                    Console.WriteLine($"{counter * number} / {number} = {counter}.");
                }
            }

            goto Start;
        }
    }
}
