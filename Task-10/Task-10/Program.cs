﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;

            Start:

            var years = new List<int>();

            Console.WriteLine("What year is it?");

            if (int.TryParse(Console.ReadLine(), out year))
            {
                for (int counter = 0; counter < 21; counter++)
                {
                    if (year % 4 == 0 && (year % 400 == 0 || year % 100 > 0))
                    {
                        years.Add(year);
                    }
                    year++;
                }

                Console.WriteLine($"There is a total of {years.Count} leap years in the next 20 years. These are the leap years:");
                years.ForEach(Console.WriteLine);
            }

            goto Start;
        }
    }
}
