﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {

            var credits = 15;
            var weeks = 12;
            var contact = 5;

            Console.WriteLine($"How many credits is the course? (Press the 'enter' key to enter the default value of {credits}).");
            credits = Check(credits);

            Console.WriteLine($"How many weeks long is the course? (Press the 'enter' key to enter the default value of {weeks}).");
            weeks = Check(weeks);

            Console.WriteLine($"How many hours do you spend in class, per week, for this course? (Press the 'enter' key to enter the default value of {contact}).");
            contact = Check(contact);

            double total = credits * 10;
            var totalcontact = contact * weeks;
            double totalhome = total - totalcontact;

            Console.WriteLine($"The course will require a total of {total} hours study, which is {total/weeks} hours of study a week.");
            Console.WriteLine($"Of this, you indicated you will do a total of {totalcontact} hours study in CLASS, which is {contact} hours of CLASS study a week.");
            Console.WriteLine($"This means you will need to study for a total of {totalhome} hours at HOME, which is {totalhome/weeks} hours of HOME study a week.");

        }

        static int Check(int defaults)
        {
            int number;

        Retry:

            var answer = Console.ReadLine();

            if (int.TryParse(answer, out number))
            {
                return number;
            }
            else if (answer == "")
            {
                number = defaults;
                return number;
            }
            else
            {
                Console.WriteLine($"That is not a valid input. Please input another number or press the 'enter' key to use the default value of {defaults}.");
                goto Retry;
            }
        }
    }
}
