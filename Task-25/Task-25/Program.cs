﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;

        Start:

            Console.WriteLine("Please enter a number.");
            string readline = Console.ReadLine();

            if (double.TryParse(readline, out number))
            {
                Console.WriteLine($"You entered the number {number}.");
            }
            else
            {
                Console.WriteLine($"You didn't enter a number. Instead, you entered '{readline}'.");
            }

            goto Start;
        }
    }
}
