﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = new List<double>();
            double number;

            for (int counter = 1; counter < 6; counter++)
            {
                Retry:
         
                Console.WriteLine($"Please input a number for input number {counter}.");

                if (double.TryParse(Console.ReadLine(), out number))
                {
                    inputs.Add(number);
                }
                else
                {
                    Console.WriteLine("That is not a valid number.");
                    goto Retry;
                }
            }

            Console.WriteLine($"{inputs[0]}+{inputs[1]}+{inputs[2]}+{inputs[3]}+{inputs[4]} = {inputs.Sum()}");
        }
    }
}
