﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            double number1;
            double number2;

        Start:

            Console.WriteLine("Please enter 2 numbers. One on each line.");

            if (double.TryParse(Console.ReadLine(), out number1) && double.TryParse(Console.ReadLine(), out number2))
            {
                Console.WriteLine($"When added as numbers: {number1} + {number2} = {number1 + number2}.");
                Console.WriteLine($"When added as a string: {number1} + {number2} = {number1.ToString() + number2.ToString()}");
            }
            else
            {
                Console.WriteLine("One of your numbers was incorrect. Please try again.");
            }

            goto Start;
        }
    }
}
