﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double number1;
            double number2;
            double number3;

            Start:

            Console.WriteLine("Please enter the prices of 3 items. (One per line, Numbers only).");

            if (double.TryParse(Console.ReadLine(), out number1) && double.TryParse(Console.ReadLine(), out number2) && double.TryParse(Console.ReadLine(), out number3))
            {
                var total = (number1 + number2 + number3) * 1.15;
                Console.WriteLine($"${number1} + ${number2} + ${number3} + GST = ${total}.");
                Console.WriteLine($"The total cost of the 3 items is ${total}.");
            }
            else
            {
                Console.WriteLine("Your previous entry was not a number. Please start again.");
            }

            goto Start;
        }
    }
}
