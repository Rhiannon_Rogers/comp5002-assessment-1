﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;
            const double conversion = 1.609344;

            Start:

            Console.WriteLine("Please enter the number you wish to convert to km or miles. (Type numbers only).");

            if (double.TryParse(Console.ReadLine(), out number))
            {
                Menu:
                Console.WriteLine("**************************************************************************");
                Console.WriteLine("Do you wish to convert to km or miles? (Type '1' for km or '2' for Miles).");
                Console.WriteLine("1. km");
                Console.WriteLine("2. Miles");
                Console.WriteLine("**************************************************************************");

                var type = Console.ReadLine();

                if (type == "1")
                {
                    Console.WriteLine($"{number} miles is {number * conversion}km.");
                }
                else if (type == "2")
                {
                    Console.WriteLine($"{number}km is {number / conversion} miles.");
                }
                else
                {
                    Console.WriteLine("You did not give me a valid response.");
                    goto Menu;
                }
            }

            goto Start;
        }
    }
}
