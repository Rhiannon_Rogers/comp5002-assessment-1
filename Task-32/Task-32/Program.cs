﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {
            var weeks = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
            for (int counter = 0; counter < 7; counter++)
            {
                Console.WriteLine($"{weeks[counter]} is day {counter + 1}");
            }
        }
    }
}
