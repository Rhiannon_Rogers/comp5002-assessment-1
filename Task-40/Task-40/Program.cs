﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {

            int input;
            var weeks = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

        Start:

            int wednesdays = 0;
            int counter = 0;

            Console.WriteLine("How many days are in this month? (enter a number from 28-31).");
            
            if (int.TryParse(Console.ReadLine(), out input))
            {

                for (int day = 0; day < input; day++)
                {
                    counter++;
                    Console.WriteLine($"Day {day + 1} is a {weeks[counter - 1]}.");

                    if (counter == 3)
                    {
                        wednesdays++;
                    }
                    else if (counter == 7)
                    {
                        counter = 0;
                    }
                }
                Console.WriteLine($"There are a total of {wednesdays} wednesdays in a month with {input} days, assuming the month starts on a monday.");
            }

            goto Start;
        }
    }
}
