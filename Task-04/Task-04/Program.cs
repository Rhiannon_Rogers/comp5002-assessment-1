﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;

            Start:

            Console.WriteLine("Please enter the number you wish to convert to fahrenheit or celsius. (Type numbers only).");

            if (double.TryParse(Console.ReadLine(), out number))
            {
                Menu:
                Console.WriteLine("**********************************************************************************************");
                Console.WriteLine("Do you wish to convert to fahrenheit or celsius? (Type 1 for 'fahrenheit' or 2 for 'celsius').");
                Console.WriteLine("1. Fahrenheit");
                Console.WriteLine("2. Celsius");
                Console.WriteLine("**********************************************************************************************");

                var type = Console.ReadLine();

                if (type == "1")
                {
                    Console.WriteLine($"{number} celsius is {(number * 1.8) + 32} fahrenheit");
                }
                else if (type == "2")
                {
                    Console.WriteLine($"{number} fahrenheit is {((number - 32) * 5) / 9} celsius");
                }
                else
                {
                    Console.WriteLine("You did not give me a valid response.");
                    goto Menu;
                }
            }

            goto Start;
        }
    }
}
