﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = new Dictionary<string, int>
            {
                { "January", 31 },
                { "February", 28 },
                { "March", 31 },
                { "April", 30 },
                { "May", 31 },
                { "June", 30 },
                { "July", 31 },
                { "August", 31 },
                { "September", 30 },
                { "October", 31 },
                { "November", 30 },
                { "December", 31 }
            };

            Console.WriteLine("The following months have 31 days:");

            foreach (var entry in year)
            {
                if (entry.Value == 31) 
                {
                    Console.WriteLine($"{entry.Key}");
                }
            }

        }
    }
}
