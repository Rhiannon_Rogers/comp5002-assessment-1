﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            int input;

        Start:

            Console.WriteLine("How many times should I loop? (Please enter a valid whole number).");

            if (int.TryParse(Console.ReadLine(), out input))
            {
                for (int counter = 1; counter < input+1; counter++)
                {
                    if (counter == input)
                    {
                        Console.WriteLine($"The current loop count is {counter}. The loop count is now equal to the value of {input}. The count is complete.");
                    }
                    else
                    {
                        Console.WriteLine($"The current loop count is {counter}. The loop count is not yet equal to the value of {input}.");
                    }
                }
            }

            goto Start;
        }
    }
}
