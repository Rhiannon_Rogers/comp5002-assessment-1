﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;

        Start:

            Console.WriteLine("Please enter a number.");

            if (double.TryParse(Console.ReadLine(), out number))
            {
                if (number % 4 == 0 && number % 3 == 0)
                {
                    Console.WriteLine($"Congratulations, the number {number} is fully divisible by both 3 and 4.");
                }
                else if (number % 4 == 0)
                {
                    Console.WriteLine($"The number {number} is only divisible by 4.");
                }
                else if (number % 3 == 0)
                {
                    Console.WriteLine($"The number {number} is only divisible by 3.");
                }
                else
                {
                    Console.WriteLine($"Sorry, the number {number} is not divisible by 3 or 4.");
                }
            }

            goto Start;
        }
    }
}
